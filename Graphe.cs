﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace TPRAM
{
    public class Graphe
    {
        //Declaration des variables necessaire pour designer le graphe 
        private List<int> matrice;// le variable qui représent la matrice de valuation
        private int nbrSommets; //le variable qui représente le nombre des sommets
        private List<List<int>> elementsConnexe;
        private int nbrdeselementsConnex;//le variable qui représent le nombre d'composants connexe

        //Default Constructor
        
        public Graphe(int nbrSommets)
        {
            Matrice = new List<int>();
            elementsConnexe = new List<List<int>>();
            NbrSommets = nbrSommets;
            CreateMatrice();
        }

        // Les proprieter qui less l'utilisateur modifier les variable marquer Private
        // La proprieter qui afficher la matrice de valuation
        public int NbrdeselementConnex
        {
            get { return nbrdeselementsConnex; }
            set { nbrdeselementsConnex = value; }
        }


        public List<int> Matrice
        {
            get { return matrice; }
            set { matrice=value; }
        }

        // La proprieter qui afficher le nombre des sommets et le modifier
        public int NbrSommets
        {
            get { return nbrSommets; }
            set { nbrSommets = value; }
        }

        // La proprieter qui afficher l'element connexe
        public List<List<int>> ElementsConnex
        {
            get { return elementsConnexe; }
        }
        //La procédure qui creé la matrice de valuation
        private void CreateMatrice()
        {
            for (int i = 0, c = (nbrSommets * nbrSommets); i < c; i++)
            {
                matrice.Add(0);
            }
        }

        // Trouver les elements connexes
        public void CConnexe()
        {
            elementsConnexe.Add(new List<int>() { 0 });
            nbrdeselementsConnex = 0;
            int finElement = (int)(nbrSommets - 1);
            int indexElementConnexe = 0;
            List<int> tousSommet = new List<int>() { 0 };
            while (finElement > 0)
            {
                int c = (int)elementsConnexe.ElementAt<List<int>>(indexElementConnexe).Count();
                int i = 0;
                int i1;
                while (i < c && finElement != 0)
                {
                    i1 = (int)(elementsConnexe.ElementAt<List<int>>(indexElementConnexe).ElementAt<int>(i) * nbrSommets);
                    for (int j = 0; j < nbrSommets; j++)
                    {
                        if (matrice.ElementAt<int>(i1 + j) >0)
                        {
                            // Test if the element i1 is already in the list or not
                            Boolean exist = false;
                            foreach (int condition in tousSommet)
                            {
                                if (j == condition)
                                {
                                    exist = true;
                                    break;
                                }
                            }

                            if (!exist)
                            {
                                finElement--;
                                elementsConnexe.ElementAt<List<int>>(indexElementConnexe).Add(j);
                                tousSommet.Add(j);
                                tousSommet.Sort();
                                elementsConnexe.ElementAt<List<int>>(indexElementConnexe).Sort();

                            } // End if

                        }
                    }
                    i++;
                    c = (int)elementsConnexe.ElementAt<List<int>>(indexElementConnexe).Count();
                }

                int j1 = tousSommet.ElementAt<int>(0);
                Boolean exist1 = true;

                while (exist1 && (j1 < nbrSommets) && (finElement > 0))
                {
                    foreach (int condition in tousSommet)
                    {
                        if (j1 == condition)
                        {
                            j1++;
                        }
                        else
                        {
                            exist1 = false;
                            break;
                        }

                    }

                }
                if (finElement > 1)
                {
                    indexElementConnexe++;
                    elementsConnexe.Add(new List<int>() { j1 });
                    nbrdeselementsConnex++;
                    tousSommet.Add(j1);
                    tousSommet.Sort();
                    finElement--;
                }
                else if (finElement == 1)
                {
                    indexElementConnexe++;
                    elementsConnexe.Add(new List<int>() { j1 });
                    nbrdeselementsConnex++;
                    tousSommet.Add(j1);
                    tousSommet.Sort();
                    finElement--;
                }
            } // Fin While

            nbrdeselementsConnex = elementsConnexe.Count; 
        } // Fin Function
    }
}
