﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Threading;
using System.Diagnostics;
using System.Threading;

namespace TPRAM
{
    public class Kruskal
    {
        private List<int> matriceMin;
        private List<int> matriceMax;
        private List<int> matriceARMin;
        private List<int> matriceARMax;
        private String kruskalMinTime;
        private String kruskalMaxTime;
        private String kruskalMinCout;
        private String kruskalMaxCout;

        public Kruskal(List<int> matriceAR)
        {
            matriceMin = matriceAR;
            matriceMax = matriceAR;
        }

        public List<int> MatriceARMin
        {
            get { return matriceARMin; }
        }

        public List<int> MatriceARMax
        {
            get { return matriceARMax; }
        }

        public String KruskalMinTime
        {
            get { return kruskalMinTime; }
        }

        public String KruskalMaxTime
        {
            get { return kruskalMaxTime; }
        }

        public String KruskalMinCout
        {
            get { return kruskalMinCout; }
        }

        public String KruskalMaxCout
        {
            get { return kruskalMaxCout; }
        }

        public void ExecuterKruskalMinAlgorithm()
        {
            matriceARMin = new List<int>();
            kruskalMinTime = "";
            kruskalMinCout = "";
            Stopwatch stopWatchKMin = new Stopwatch();
            
            stopWatchKMin.Start();              
            ImplimentationDesAlgorithmes.Kruskal(matriceMin, ref matriceARMin, ref kruskalMinCout);
            stopWatchKMin.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan tsKMin = stopWatchKMin.Elapsed;

            kruskalMinTime = Convert.ToString(tsKMin.Milliseconds) + " ms";
            
        }

        public void ExecuterKruskalMaxAlgorithm()
        {
            matriceARMax = new List<int>();
            kruskalMaxTime = "";
            kruskalMaxCout = "";
            Stopwatch stopWatchKMax = new Stopwatch();

            stopWatchKMax.Start();
            
            ImplimentationDesAlgorithmes.KruskalMax(matriceMax, ref matriceARMax, ref kruskalMaxCout);

            stopWatchKMax.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan tsKMax = stopWatchKMax.Elapsed;

            kruskalMaxTime = Convert.ToString(tsKMax.Milliseconds) + " ms";
        }
    }
}
