﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TPRAM
{
    class Prim
    {
        private List<int> matriceMin;
        private List<int> matriceMax;
        private List<int> matriceARMin;
        private List<int> matriceARMax;
        private String primMinTime;
        private String primMaxTime;
        private String primMinCout;
        private String primMaxCout;

        public Prim(List<int> matriceAR)
        {
            matriceMin = matriceAR;
            matriceMax = matriceAR;
        }

        public List<int> MatriceARMin
        {
            get{return matriceARMin;}
        }

        public List<int> MatriceARMax
        {
            get { return matriceARMax; }
        }

        public String PrimMinTime
        {
            get { return primMinTime; }
        }

        public String PrimMaxTime
        {
            get { return primMaxTime; }
        }

        public String PrimMinCout
        {
            get { return primMinCout; }
        }

        public String PrimMaxCout
        {
            get { return primMaxCout; }
        }

        public void ExecuterPrimMinAlgorithm()
        {
            matriceARMin = new List<int>();
            primMinTime = "";
            primMinCout = "";
            Stopwatch stopWatchPMin = new Stopwatch();

            stopWatchPMin.Start();

            ImplimentationDesAlgorithmes.Prim(matriceMin, ref matriceARMin, ref primMinCout);

            stopWatchPMin.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan tsPMin = stopWatchPMin.Elapsed;

            
            primMinTime = Convert.ToString(tsPMin.Milliseconds) + " ms";
        }

        public void ExecuterPrimMaxAlgorithm()
        {
            matriceARMax = new List<int>();
            primMaxTime = "";
            primMaxCout = "";

            Stopwatch stopWatchPMax = new Stopwatch();

            stopWatchPMax.Start();

            ImplimentationDesAlgorithmes.PrimMax(matriceMax, ref matriceARMax, ref primMaxCout);


            stopWatchPMax.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan tsPMax = stopWatchPMax.Elapsed;

            primMaxTime = Convert.ToString(tsPMax.Milliseconds) + " ms";
        }
    }
}
