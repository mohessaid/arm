﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TPRAM
{
    static class Operation_Sur_La_Matrice_De_Valuation
    {
        // Creation de la tmatrice 
        public static void CreateTMatrice(List<int> matrice, ref List<int> matriceT)
        {
            int nbrSommets = Convert.Toint(Math.Sqrt(matrice.Count));
            int c = Convert.Toint(matrice.Count);
            for (int i = 0; i < nbrSommets; i++)
            {
                for (int j = 0; j < c; j += nbrSommets)
                {
                    matriceT.Add(matrice.ElementAt<int>(i + j));
                }
            }
        }
    }
}
