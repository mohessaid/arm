﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Threading;
using System.Timers;
using System.Diagnostics;

namespace TPRAM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Variable pour enrigistrer les graphe non orienter
        // Krusal variable
        List<Label> graphSommetK;
        List<Line> graphArretK;
        List<Label> graphValeurK;

        List<Label> graphSommetKMin;
        List<Line> graphArretKMin;
        List<Label> graphValeurKMin;

        List<Label> graphSommetKMax;
        List<Line> graphArretKMax;
        List<Label> graphValeurKMax;

        // Prim variable
        List<Label> graphSommetP;
        List<Line> graphArretP;
        List<Label> graphValeurP;

        List<Label> graphSommetPMin;
        List<Line> graphArretPMin;
        List<Label> graphValeurPMin;

        List<Label> graphSommetPMax;
        List<Line> graphArretPMax;
        List<Label> graphValeurPMax;

        // Sollin variable
        List<Label> graphSommetS;
        List<Line> graphArretS;
        List<Label> graphValeurS;

        List<Label> graphSommetSMin;
        List<Line> graphArretSMin;
        List<Label> graphValeurSMin;

        List<Label> graphSommetSMax;
        List<Line> graphArretSMax;
        List<Label> graphValeurSMax;

        List<Grid> interfaceorder;
        Graphe Gr;
        Graphe Gr1;
  
        List<int> matrice; // Le variable qui occupe de modification de la matrice
        List<int> matriceKARMin;
        List<int> matriceKARMax;

        List<int> matricePARMin;
        List<int> matricePARMax;

        List<int> matriceSARMin;
        List<int> matriceSARMax;

        Kruskal kruskalObject;
        Prim primObject;
        Sollin sollinObject;          

        String affMatrice; // Le variable qui affiche la matrice dans l'interface

        List<UInt16> zoomState;
        List<Double> demansionGraphBoardK;
        List<Double> demansionGraphBoardP;
        List<Double> demansionGraphBoardS;
        public MainWindow()
        {
            InitializeComponent();

            interfaceorder = new List<Grid>() 
            { BackgroundImage, DailogBox,
              MatriceValuation, MessageBoxAttention, 
              MessageBoxErreur, GrapheInterface 
            };
            affMatrice = "";
            zoomState = new List<UInt16>() { 0, 0, 0 };
        }

        #region Gestion des evenment de Menu

        private void nouveauGraphe_Click(object sender, EventArgs e)
        {
            
            DailogBox.SetValue(Panel.ZIndexProperty, 4);
            BackgroundImage.SetValue(Panel.ZIndexProperty, 3);
            
        }

        private void ouvrir_Click(object sender, EventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ovrir = new Microsoft.Win32.OpenFileDialog();
            ovrir.AddExtension = true;
            ovrir.Filter = "Graphe|*.grf|All Files|*.*";
            ovrir.ShowDialog();
            MessageBox.Show(ovrir.FileName);
            BinaryReader binReader = new BinaryReader(File.Open(ovrir.FileName, FileMode.Open));
            matrice = new List<int>();
            int stopR = 0;
            int cout = binReader.ReadInt32();
            for (int i = 0; i < cout; i++)
            {
                stopR = binReader.ReadInt32();
                matrice.Add(stopR);
            }

            binReader.Close();

            ValiderLesChangement_Click(null, null);

            
        }
        private void enrigistrer_Click(object sender, EventArgs e)
        {
               
        }
        private void enrigistrerSous_Click(object sender, EventArgs e)
        {
            // Configure save file dialog box
            Microsoft.Win32.SaveFileDialog save = new Microsoft.Win32.SaveFileDialog();
            save.FileName = "Document"; // Default file name
            save.DefaultExt = ".grf"; // Default file extension
            save.Filter = "Graphe|*.grf|All Files|*.*"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = save.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = save.FileName;
                
                BinaryWriter binWriter = new BinaryWriter(File.Open(filename, FileMode.Create));
                binWriter.Write(matrice.Count);
                for (int i = 0, c = matrice.Count; i < c; i++ )
                    binWriter.Write(matrice[i]);

                binWriter.Close();
            }
        }
        private void imprimer_Click(object sender, EventArgs e)
        {
            
        }
        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        // Le messagebox Attention
        private void BOK_Click(object sender, EventArgs e)
        {
            MessageBoxAttention.SetValue(Panel.ZIndexProperty, 1);
        }

        private void OKErreur_Click(object sender, EventArgs e)
        {
            MessageBoxErreur.SetValue(Panel.ZIndexProperty, 1);
        }

        #region Gestion des evenement de DialogBox

        private void TextChangedEventHandler(object sender, TextChangedEventArgs args)
        {
            TextBox input = sender as TextBox;
            int i;
            if (input.Text == "")
            {
                input.Background = Brushes.White;
                input.Foreground = Brushes.Black;
                ErrorDonnees.Content = "Il faut entrer un nombre";
                OKButton.IsEnabled = false;
            }
            else
            {
                try
                {
                    i = int.Parse((input.Text));
                }
                catch (FormatException exception)
                {
                    input.BorderBrush = Brushes.Red;
                    input.Background = Brushes.Red;
                    input.Foreground = Brushes.White;
                    ErrorDonnees.Content = "Que Des Nombre!";
                    OKButton.IsEnabled = false;
                }
                catch (OverflowException exception)
                {
                    input.BorderBrush = Brushes.Violet;
                    input.Background = Brushes.Violet;
                    input.Foreground = Brushes.White;
                    ErrorDonnees.Content = "Un Nombre Trés Grand";
                    OKButton.IsEnabled = false;
                }
                finally
                {
                    if (int.TryParse(input.Text,out i))
                    {
                        input.Background = Brushes.White;
                        input.Foreground = Brushes.Black;
                        ErrorDonnees.Content = "Trés Bon!!!!";
                        OKButton.IsEnabled = true;
                    }
                }
                    
            }
        }
        private void OKButton_Click(object sender, EventArgs e)
        {
            Gr1 = new Graphe(int.Parse(OrdreGraphe.Text));
            // Affichage De Matrice Sur L'interface
            matrice = new List<int>( Gr1.Matrice);
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            AffColRow(Gr1.NbrSommets);
            elementList();



            DailogBox.SetValue(Panel.ZIndexProperty, 0);
            BackgroundImage.SetValue(Panel.ZIndexProperty, 1);
            MatriceValuation.SetValue(Panel.ZIndexProperty, 3);
        }
        private void CancelButton_Click(object sender, EventArgs e)
        {
            DailogBox.SetValue(Panel.ZIndexProperty, 0);
        }

        #endregion

        #region les procédures pour remplir la matrice de valuation 

        // Ajouter un somments
        private void AddSomments()
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            List<int> matriceClone = new List<int>();

            for (int i = 0, c = matrice.Count; i < c; i += numDesSomments)
            {
                for (int j = 0; j < numDesSomments; j++)
                {
                    matriceClone.Add(matrice.ElementAt<int>(j + i));
                }
                matriceClone.Add(0);
            }

            for (int i = 0, c = (numDesSomments + 1); i < c; i++)
            {
                matriceClone.Add(0);
            }

            matrice = matriceClone;
        }

        // Supprimer un Sommets
        private void SuppSommets()
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            List<int> matriceClone = new List<int>();

            for (int i = 0, c = (matrice.Count - numDesSomments); i < c; i += numDesSomments)
            {
                for (int j = 0, cj = (numDesSomments - 1); j < cj; j++)
                {
                    matriceClone.Add(matrice.ElementAt<int>(j + i));
                }
            }
            matrice = matriceClone;
        }

        // Pour remplir les combox
        private void elementList()
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            ComboBoxColone.Items.Clear();
            ComboBoxLine.Items.Clear();
            for (int i = 0; i < numDesSomments; i++)
            {
                ComboBoxColone.Items.Add(i);
                ComboBoxLine.Items.Add(i);
            }
        }

        // La methode qui traiter l'evenement de clique sur le button +
        private void btnAddMI_Click(object sender, EventArgs e)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            AddSomments();
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            rowIndices.Text += numDesSomments + "\n\n" + " ";
            colIndices.Text += numDesSomments + "\t";
            elementList();

        }

        // La methode qui traiter l'evenement de clique sur le button -
        private void btnRemoveMI_Click(object sender, EventArgs e)
        {

            SuppSommets();
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            AffColRow(numDesSomments);
            elementList();
        }

        // Composer la matrice sous un chaine bien former
        private void ComposeMatriceString()
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            affMatrice = "";
            for (int i = 0, c = matrice.Count; i < c; i += numDesSomments)
            {
                for (int j = 0; j < numDesSomments; j++)
                {
                    affMatrice += matrice.ElementAt<int>(j + i) + "\t";
                }
                affMatrice += "\n\n";
            }
        }

        // La methode qui occupe de la affichage des indices dans la grid

        private void AffColRow(int prm1)
        {
            colIndices.Text = " ";
            rowIndices.Text = " ";
            for (int i = 0; i < prm1; i++)
            {
                rowIndices.Text += i + "\n\n" + " ";
                colIndices.Text += i + "\t";
            }
        }

        // la procedure qui metre tous les éléments de la diagonale à 0
        private void Diagonale(ref List<int> matrice)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            for (int i = 0; i < matrice.Count; i += (numDesSomments + 1))
            {
                matrice[i] = 0;
            }
        }

        // le procédure qui metre les élement symétrique de la MatericeDeValuation à 0
        private void symetrique(ref List<int> matrice)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            int x = 1;
            while (x < numDesSomments)
            {
                for (int i = x * numDesSomments; i < x * (numDesSomments + 1); i++)
                {
                    matrice[i] = 0;
                }
                x++;
            }
            
        }

        // procedure qui affecte la valeur 1 à la matrice
        private void RemplerMatrice(ref List<int> matrice, int x, int y)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            int valeur = int.Parse(ValeurTextBox.Text);
            if (radioBtnCellule.IsChecked == true)
            {
                matrice[(x * numDesSomments) + y] = valeur;
                matrice[(y * numDesSomments) + x] = valeur;
               
            }
            else if (radioBtnLine.IsChecked == true)
            {
                for (int i = x * numDesSomments; i < numDesSomments * (x + 1); i++)
                {
                    matrice[i] = valeur;
                }
                for (int i = x, j = x; i < numDesSomments * numDesSomments; j++, i += numDesSomments)
                {
                    if (j == i)
                    {
                        matrice[i] = 0;
                    }
                    matrice[i] = valeur;
                }
            }
            else if (radioBtnColone.IsChecked == true)
            {
                for (int i = y; i < numDesSomments * numDesSomments; i += numDesSomments)
                {
                    matrice[i] = valeur;
                }
                for (int i = y * numDesSomments; i < numDesSomments * (y + 1); i++)
                {
                    matrice[i] = valeur;
                }
            }
            Diagonale(ref matrice);
            symetrique(ref matrice);
        }

        // La procédure qui Laod Une Random Matrice 
        private void randomMatrice(ref List<int>matrice)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            Random valeur = new Random();
            for (int i = 0; i < numDesSomments*numDesSomments; i++)
            {
                int x = valeur.Next(0,100);
                matrice[i] = x;
            }
            Diagonale(ref matrice);
            symetrique(ref matrice);
        }


        #endregion

        #region Matrice De Valuation

        private void ValiderButton_Click(object sender, EventArgs e)
        {
            try
            {
                int x = 0;
                int y = 0;

                if (radioBtnLine.IsChecked == true || radioBtnCellule.IsChecked == true)
                {
                    x = Convert.ToUInt16(ComboBoxLine.Text);
                }
                if (radioBtnCellule.IsChecked == true || radioBtnColone.IsChecked == true)
                {
                    y = Convert.ToUInt16(ComboBoxColone.Text);
                }

                RemplerMatrice(ref matrice, x, y);

            }
            catch (FormatException exception)
            {
                if (ComboBoxColone.Text == "" && ComboBoxLine.Text == "")
                {
                    LabelErreur.Content = "Il faut selectinné Une ligne ou une Colone";
                }
                else if (ComboBoxColone.Text == "")
                {
                    LabelErreur.Content = "Il faut sélectionné une Colone";
                }
                else if (ComboBoxLine.Text == "")
                {
                    LabelErreur.Content = "Il faut sélectionné une Line";

                }
                
                MatriceValuation.SetValue(Panel.ZIndexProperty, 3);
                MessageBoxErreur.SetValue(Panel.ZIndexProperty, 4);
            }

            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            AffColRow(numDesSomments);
        }

        private void TextChanged1EventHandler(object sender, TextChangedEventArgs args)
        {
            TextBox input = sender as TextBox;
            int i;
            if (input.Text == "")
            {
                input.Background = Brushes.White;
                input.Foreground = Brushes.Black;
                ErrorDonnees.Content = "Il faut entrer un nombre";
                OKButton.IsEnabled = false;
            }
            else
            {
                try
                {
                    i = Convert.ToUInt16(input.Text);
                }
                catch (FormatException exception)
                {
                    input.BorderBrush = Brushes.Red;
                    input.Background = Brushes.Red;
                    input.Foreground = Brushes.White;
                    ValiderButton.IsEnabled = false;
                }
                catch (OverflowException exception)
                {
                    input.BorderBrush = Brushes.Violet;
                    input.Background = Brushes.Violet;
                    input.Foreground = Brushes.White;
                    ValiderButton.IsEnabled = false;
                }
                finally
                {
                    if (int.TryParse(input.Text, out i))
                    {
                        input.Background = Brushes.White;
                        input.Foreground = Brushes.Black;
                        ValiderButton.IsEnabled = true;
                    }
                }

            }
        }

        // le Button Laod Matrice
        private void LoadMatrice_Click(object sender, EventArgs e)
        {
            randomMatrice(ref matrice);
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
        }

        private void graph()
        {
          
            

            //Designer Le Graphe
            graphSommetS = new List<Label>();
            graphArretS = new List<Line>();
            graphValeurS = new List<Label>();

            graphSommetSMin = new List<Label>();
            graphArretSMin = new List<Line>();
            graphValeurSMin = new List<Label>();

            graphSommetSMax = new List<Label>();
            graphArretSMax = new List<Line>();
            graphValeurSMax = new List<Label>();

            
           

        }

        private void graphK(int i)
        {


            graphBoardK.Children.Clear();

            //Designer Le Graphe
            graphSommetK = new List<Label>();
            graphArretK = new List<Line>();
            graphValeurK = new List<Label>();

            graphSommetKMin = new List<Label>();
            graphArretKMin = new List<Line>();
            graphValeurKMin = new List<Label>();

            graphSommetKMax = new List<Label>();
            graphArretKMax = new List<Line>();
            graphValeurKMax = new List<Label>();

            OperationSurLeGraphe.SommetArretePositionGNO(matrice, ref graphArretK, ref graphSommetK, ref graphValeurK);
            OperationSurLeGraphe.StylerSommets(ref graphSommetK, "sommetTemplate");
            OperationSurLeGraphe.StylerSommets(ref graphValeurK, "valeurTemplate");

            graphBoardK.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphSommetK.ElementAt<Label>(0)) * 2);
            graphBoardK.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphSommetK.ElementAt<Label>(0)) * 2);
            

            if (i == 0)
            {
                
                foreach (Line l in graphArretK)
                    graphBoardK.Children.Add(l);
               
                foreach (Label l in graphSommetK)
                    graphBoardK.Children.Add(l);

                foreach (Label l in graphValeurK)
                    graphBoardK.Children.Add(l);
            }
            else if (i == 1)
            {
                OperationSurLeGraphe.SommetArretePositionGNO(matriceKARMin, ref graphArretKMin, ref graphSommetKMin, ref graphValeurKMin);
                foreach (Line l in graphArretK)
                {
                    Boolean equal = false;
                    foreach (Line lo in graphArretKMin)
                    {
                        if ((lo.X1 == l.X1) && (lo.X2 == l.X2) && (lo.Y1 == l.Y1) && (lo.Y2 == l.Y2))
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.Stroke = Brushes.Green;
                        graphBoardK.Children.Add(l);
                    }
                    else
                    {
                        graphBoardK.Children.Add(l);
                    }
                }

                foreach (Label l in graphSommetK)
                    graphBoardK.Children.Add(l);

                foreach (Label l in graphValeurK)
                {
                    Boolean equal = false;
                    foreach (Label lo in graphValeurKMin)
                    {
                        if (lo.Name == l.Name)
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.SetResourceReference(Window.TemplateProperty, "valeurMinTemplate");
                        graphBoardK.Children.Add(l);
                    }
                    else
                    {
                        graphBoardK.Children.Add(l);
                    }
                }
            }
            else
            {
                OperationSurLeGraphe.SommetArretePositionGNO(matriceKARMax, ref graphArretKMax, ref graphSommetKMax, ref graphValeurKMax);
                foreach (Line l in graphArretK)
                {
                    Boolean equal = false;
                    foreach (Line lo in graphArretKMax)
                    {
                        if ((lo.X1 == l.X1) && (lo.X2 == l.X2) && (lo.Y1 == l.Y1) && (lo.Y2 == l.Y2))
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.Stroke = Brushes.Red;
                        graphBoardK.Children.Add(l);
                    }
                    else
                    {
                        graphBoardK.Children.Add(l);
                    }
                }

                foreach (Label l in graphSommetK)
                    graphBoardK.Children.Add(l);

                foreach (Label l in graphValeurK)
                {
                    Boolean equal = false;
                    foreach (Label lo in graphValeurKMax)
                    {
                        if (lo.Name == l.Name)
                        {
                            equal = true;
                            break;
                        }
                    }
                    if (equal)
                    {
                        l.SetResourceReference(Window.TemplateProperty, "valeurMaxTemplate");
                        graphBoardK.Children.Add(l);
                    }
                    else
                    {
                        graphBoardK.Children.Add(l);
                    }
                }
            }
            

        }

        private void graphP(int i)
        {


            graphBoardP.Children.Clear();

            //Designer Le Graphe
            graphSommetP = new List<Label>();
            graphArretP = new List<Line>();
            graphValeurP = new List<Label>();

            graphSommetPMin = new List<Label>();
            graphArretPMin = new List<Line>();
            graphValeurPMin = new List<Label>();

            graphSommetPMax = new List<Label>();
            graphArretPMax = new List<Line>();
            graphValeurPMax = new List<Label>();

            OperationSurLeGraphe.SommetArretePositionGNO(matrice, ref graphArretP, ref graphSommetP, ref graphValeurP);
            OperationSurLeGraphe.StylerSommets(ref graphSommetP, "sommetTemplate");
            OperationSurLeGraphe.StylerSommets(ref graphValeurP, "valeurTemplate");

            graphBoardP.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphSommetP.ElementAt<Label>(0)) * 2);
            graphBoardP.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphSommetP.ElementAt<Label>(0)) * 2);

            if (i == 0)
            {
                
                foreach (Line l in graphArretP)
                    graphBoardP.Children.Add(l);

                foreach (Label l in graphSommetP)
                    graphBoardP.Children.Add(l);

                foreach (Label l in graphValeurP)
                    graphBoardP.Children.Add(l);
            }
            else if (i == 1)
            {
                OperationSurLeGraphe.SommetArretePositionGNO(matricePARMin, ref graphArretPMin, ref graphSommetPMin, ref graphValeurPMin);
                
                foreach (Line l in graphArretP)
                {
                    Boolean equal = false;
                    foreach (Line lo in graphArretPMin)
                    {
                        if ((lo.X1 == l.X1) && (lo.X2 == l.X2) && (lo.Y1 == l.Y1) && (lo.Y2 == l.Y2))
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.Stroke = Brushes.Green;
                        graphBoardP.Children.Add(l);
                    }
                    else
                    {
                        graphBoardP.Children.Add(l);
                    }
                }

                foreach (Label l in graphSommetP)
                    graphBoardP.Children.Add(l);

                foreach (Label l in graphValeurP)
                {
                    Boolean equal = false;
                    foreach (Label lo in graphValeurPMin)
                    {
                        if (lo.Name == l.Name)
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.SetResourceReference(Window.TemplateProperty, "valeurMinTemplate");
                        graphBoardP.Children.Add(l);
                    }
                    else
                    {
                        graphBoardP.Children.Add(l);
                    }
                }
            }
            else
            {
                OperationSurLeGraphe.SommetArretePositionGNO(matricePARMax, ref graphArretPMax, ref graphSommetPMax, ref graphValeurPMax);
                foreach (Line l in graphArretP)
                {
                    Boolean equal = false;
                    foreach (Line lo in graphArretPMax)
                    {
                        if ((lo.X1 == l.X1) && (lo.X2 == l.X2) && (lo.Y1 == l.Y1) && (lo.Y2 == l.Y2))
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.Stroke = Brushes.Red;
                        graphBoardP.Children.Add(l);
                    }
                    else
                    {
                        graphBoardP.Children.Add(l);
                    }
                }

                foreach (Label l in graphSommetP)
                    graphBoardP.Children.Add(l);

                foreach (Label l in graphValeurP)
                {
                    Boolean equal = false;
                    foreach (Label lo in graphValeurPMax)
                    {
                        if (lo.Name == l.Name)
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.SetResourceReference(Window.TemplateProperty, "valeurMaxTemplate");
                        graphBoardP.Children.Add(l);
                    }
                    else
                    {
                        graphBoardP.Children.Add(l);
                    }
                }
            }


        }

        private void graphS(int i)
        {


            graphBoardS.Children.Clear();

            //Designer Le Graphe
            graphSommetS = new List<Label>();
            graphArretS = new List<Line>();
            graphValeurS = new List<Label>();

            graphSommetSMin = new List<Label>();
            graphArretSMin = new List<Line>();
            graphValeurSMin = new List<Label>();

            graphSommetSMax = new List<Label>();
            graphArretSMax = new List<Line>();
            graphValeurSMax = new List<Label>();

            OperationSurLeGraphe.SommetArretePositionGNO(matrice, ref graphArretS, ref graphSommetS, ref graphValeurS);
            OperationSurLeGraphe.StylerSommets(ref graphSommetS, "sommetTemplate");
            OperationSurLeGraphe.StylerSommets(ref graphValeurS, "valeurTemplate");

            graphBoardS.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphSommetS.ElementAt<Label>(0)) * 2);
            graphBoardS.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphSommetS.ElementAt<Label>(0)) * 2);

            if (i == 0)
            {

                foreach (Line l in graphArretS)
                    graphBoardS.Children.Add(l);

                foreach (Label l in graphSommetS)
                    graphBoardS.Children.Add(l);

                foreach (Label l in graphValeurS)
                    graphBoardS.Children.Add(l);
            }
            else if (i == 1)
            {
                OperationSurLeGraphe.SommetArretePositionGNO(matriceSARMin, ref graphArretSMin, ref graphSommetSMin, ref graphValeurSMin);

                foreach (Line l in graphArretS)
                {
                    Boolean equal = false;
                    foreach (Line lo in graphArretSMin)
                    {
                        if ((lo.X1 == l.X1) && (lo.X2 == l.X2) && (lo.Y1 == l.Y1) && (lo.Y2 == l.Y2))
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.Stroke = Brushes.Green;
                        graphBoardS.Children.Add(l);
                    }
                    else
                    {
                        graphBoardS.Children.Add(l);
                    }
                }

                foreach (Label l in graphSommetS)
                    graphBoardS.Children.Add(l);

                foreach (Label l in graphValeurS)
                {
                    Boolean equal = false;
                    foreach (Label lo in graphValeurSMin)
                    {
                        if (lo.Name == l.Name)
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.SetResourceReference(Window.TemplateProperty, "valeurMinTemplate");
                        graphBoardS.Children.Add(l);
                    }
                    else
                    {
                        graphBoardS.Children.Add(l);
                    }
                }
            }
            else
            {
                OperationSurLeGraphe.SommetArretePositionGNO(matriceSARMax, ref graphArretSMax, ref graphSommetSMax, ref graphValeurSMax);
                foreach (Line l in graphArretS)
                {
                    Boolean equal = false;
                    foreach (Line lo in graphArretSMax)
                    {
                        if ((lo.X1 == l.X1) && (lo.X2 == l.X2) && (lo.Y1 == l.Y1) && (lo.Y2 == l.Y2))
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.Stroke = Brushes.Red;
                        graphBoardS.Children.Add(l);
                    }
                    else
                    {
                        graphBoardS.Children.Add(l);
                    }
                }

                foreach (Label l in graphSommetS)
                    graphBoardS.Children.Add(l);

                foreach (Label l in graphValeurS)
                {
                    Boolean equal = false;
                    foreach (Label lo in graphValeurSMax)
                    {
                        if (lo.Name == l.Name)
                        {
                            equal = true;

                            break;
                        }
                    }
                    if (equal)
                    {
                        l.SetResourceReference(Window.TemplateProperty, "valeurMaxTemplate");
                        graphBoardS.Children.Add(l);
                    }
                    else
                    {
                        graphBoardS.Children.Add(l);
                    }
                }
            }


        }

        private void ValiderLesChangement_Click(object sender, EventArgs e)
        {

            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            Gr = new Graphe(numDesSomments);
            Gr.Matrice = matrice;
            Gr.NbrSommets = numDesSomments;
            Gr.CConnexe();
            

            //ImplimentationDesAlgorithmes.Kruskal(Gr.Matrice, ref matriceAr);
            if (Gr.NbrdeselementConnex == 1)
            {
                matriceKARMin = new List<int>();
                matriceKARMax = new List<int>();

                matricePARMin = new List<int>();
                matricePARMax = new List<int>();

                matriceSARMin = new List<int>();
                matriceSARMax = new List<int>();

                kruskalObject = new Kruskal(Gr.Matrice);
                primObject = new Prim(Gr.Matrice);
                sollinObject = new Sollin(Gr.Matrice);


                Thread kruskalMin = new Thread(kruskalObject.ExecuterKruskalMinAlgorithm);
                Thread kruskalMax = new Thread(kruskalObject.ExecuterKruskalMaxAlgorithm);
                Thread primMin = new Thread(primObject.ExecuterPrimMinAlgorithm);
                Thread primMax = new Thread(primObject.ExecuterPrimMaxAlgorithm);
                Thread sollinMin = new Thread(sollinObject.ExecuterSollinMinAlgorithm);
                Thread sollinMax = new Thread(sollinObject.ExecuterSollinMaxAlgorithm);
                kruskalMin.Start();
                kruskalMax.Start();
                primMin.Start();
                primMax.Start();
                sollinMin.Start();
                sollinMax.Start();

                kruskalMin.Join();
                kruskalMax.Join();
                primMin.Join();
                primMax.Join();
                sollinMin.Join();
                sollinMax.Join();

                KruskalStatus.Content = "Temps Arbre Min: " + kruskalObject.KruskalMinTime + "\n\n" +
                                        "Cout Arbre Min: " + kruskalObject.KruskalMinCout + "\n\n\n\n" +
                                        "Temps Arbre Max: " + kruskalObject.KruskalMaxTime + "\n\n" +
                                        "Cout Arbre Max: " + kruskalObject.KruskalMaxCout;

                PrimStatus.Content = "TempsArbre Min: " + primObject.PrimMinTime + "\n\n" +
                                     "Cout Arbre Min: " + primObject.PrimMinCout + "\n\n\n\n" +
                                     "Temps Arbre Max: " + primObject.PrimMaxTime + "\n\n" +
                                     "Cout Arbre Max: " + primObject.PrimMaxCout;

                SollinStatus.Content = "TempsArbre Min: " + sollinObject.SollinMinTime + "\n\n" +
                                     "Cout Arbre Min: " + sollinObject.SollinMinCout + "\n\n\n\n" +
                                     "TempsArbre Max: " + sollinObject.SollinMaxTime + "\n\n" +
                                     "Cout Arbre Max: " + sollinObject.SollinMaxCout + "\n\n\n\n";
                


                matriceKARMin = kruskalObject.MatriceARMin;
                matriceKARMax = kruskalObject.MatriceARMax;

                matricePARMin = primObject.MatriceARMin;
                matricePARMax = primObject.MatriceARMax;

                matriceSARMin = sollinObject.MatriceARMin;
                matriceSARMax = sollinObject.MatriceARMax;
                graphK(0);
                graphP(0);
                graphS(0);
                MatriceValuation.SetValue(Panel.ZIndexProperty, 3);
                //GrapheInterface.SetValue(Panel.ZIndexProperty, 4);
                AlgrithmsProgressBar.SetValue(Panel.ZIndexProperty, 4);
            }
            else
            {
                Gr.ElementsConnex.Clear();
                MatriceValuation.SetValue(Panel.ZIndexProperty, 3);
                MessageBoxAttention.SetValue(Panel.ZIndexProperty, 4);
            }
            
        }

        #endregion

        private void KruskalBtn_Click(object sender, EventArgs e)
        {
            KTab.IsSelected = true;
            PTab.IsSelected = false;
            STab.IsSelected = false;
            GrapheInterface.SetValue(Panel.ZIndexProperty, 4);
            AlgrithmsProgressBar.SetValue(Panel.ZIndexProperty, 1);
        }

        private void PrimBtn_Click(object sender, EventArgs e)
        {
            KTab.IsSelected = false;
            PTab.IsSelected = true;
            STab.IsSelected = false;
            GrapheInterface.SetValue(Panel.ZIndexProperty, 4);
            AlgrithmsProgressBar.SetValue(Panel.ZIndexProperty, 1);
        }

        private void SollinBtn_Click(object sender, EventArgs e)
        {
            KTab.IsSelected = false;
            PTab.IsSelected = false;
            STab.IsSelected = true;
            GrapheInterface.SetValue(Panel.ZIndexProperty, 4);
            AlgrithmsProgressBar.SetValue(Panel.ZIndexProperty, 1);
        }

        private void zoomGrapheK_ValueChanged(object sender, EventArgs e)
        {
            if (zoomState != null)
            {
                if (zoomState[0] == 0)
                {
                    zoomState[0] = 1;
                    demansionGraphBoardK = new List<Double>()
                {
                    GrapheHolderK.ActualHeight,
                    GrapheHolderK.ActualWidth
                };



                }
                else
                {
                    GrapheHolderK.Height = demansionGraphBoardK[0] * ZoomGrapheK.Value;
                    GrapheHolderK.Width = demansionGraphBoardK[1] * ZoomGrapheK.Value;

                }
            }
        }

        private void zoomGrapheP_ValueChanged(object sender, EventArgs e)
        {
            if (zoomState != null)
            {
                if (zoomState[1] == 0)
                {
                    zoomState[1] = 1;
                    demansionGraphBoardP = new List<Double>()
                {
                    GrapheHolderP.ActualHeight,
                    GrapheHolderP.ActualWidth
                };



                }
                else
                {
                    GrapheHolderP.Height = demansionGraphBoardP[0] * ZoomGrapheP.Value;
                    GrapheHolderP.Width = demansionGraphBoardP[1] * ZoomGrapheP.Value;

                }
            }
        }

        private void zoomGrapheS_ValueChanged(object sender, EventArgs e)
        {
            if (zoomState != null)
            {
                if (zoomState[2] == 0)
                {
                    zoomState[2] = 1;
                    demansionGraphBoardS = new List<Double>()
                {
                    GrapheHolderS.ActualHeight,
                    GrapheHolderS.ActualWidth
                };



                }
                else
                {
                    GrapheHolderS.Height = demansionGraphBoardS[0] * ZoomGrapheS.Value;
                    GrapheHolderS.Width = demansionGraphBoardS[1] * ZoomGrapheS.Value;

                }
            }
        }

        private void KT_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphK(0);
                TimeK.Content = "Le Temps d'execution: 00";
                CoutK.Content = "Le Cout: 100%";
            }
        }

        private void KMin_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphK(1);
                TimeK.Content = "Le Temps d'execution: " + kruskalObject.KruskalMinTime;
                CoutK.Content = "Le Cout: " + kruskalObject.KruskalMinCout ;
            }
        }

        private void KMax_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphK(2);
                TimeK.Content = "Le Temps d'execution: " + kruskalObject.KruskalMaxTime;
                CoutK.Content = "Le Cout: " + kruskalObject.KruskalMaxCout;
            }
        }

        private void PT_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphP(0);
                TimeP.Content = "Le Temps d'execution: 00";
                CoutP.Content = "Le Cout: 100%";
            }
        }

        private void PMin_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphP(1);
                TimeP.Content = "Le Temps d'execution: " + primObject.PrimMinTime;
                CoutP.Content = "Le Cout: " + primObject.PrimMinCout;
            }
        }

        private void PMax_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphP(2);
                TimeP.Content = "Le Temps d'execution: " + primObject.PrimMaxTime;
                CoutP.Content = "Le Cout: " + primObject.PrimMaxCout;
            }
        }

        private void ST_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphS(0);
                TimeS.Content = "Le Temps d'execution: 00";
                CoutS.Content = "Le Cout: 100%";
            }
        }

        private void SMin_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphS(1);
                TimeS.Content = "Le Temps d'execution: " + sollinObject.SollinMinTime;
                CoutS.Content = "Le Cout: " + sollinObject.SollinMinCout;
            }
        }

        private void SMax_Checked(object sender, EventArgs e)
        {
            if (matrice != null)
            {
                graphS(2);
                TimeS.Content = "Le Temps d'execution: " + sollinObject.SollinMaxTime;
                CoutS.Content = "Le Cout: " + sollinObject.SollinMaxCout;
            }
        }
    }
}
