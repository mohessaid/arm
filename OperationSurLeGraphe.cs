﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Media;
using System.Windows.Media;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Windows.Documents;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TPRAM
{
    static class OperationSurLeGraphe
    {
        public static void arbreNiveau(List<int> matrice, ref Dictionary<Int16, Int16> niveau)
        {
            int nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            for (int i = 0, nbr = 0; i < nbrSommets; i += nbrSommets, nbr++)
            {
                niveau.Add(Convert.ToInt16(nbr), Convert.ToInt16(0));
                for (int j = 0; j < nbrSommets; j++)
                {

                    if (matrice[i + j] != 0)
                    {
                        niveau[Convert.ToInt16(nbr)]++;
                    }
                }
            }

            niveau = (from entry in niveau orderby entry.Value descending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);
        }
        public static void SommetArretePositionGNO1(List<int> matrice, ref List<Line> arrets, ref List<Label> sommets, ref List<Label> Valeurs)
        {
            Dictionary<Int16, Int16> niveau = new Dictionary<Int16, Int16>();
            arbreNiveau(matrice, ref niveau);
            
            int nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            double size = (niveau.ElementAt(0).Value + 200);
            double x1 = size / 2;
            double y1 = 100;
            

            for (int i = 0; i < nbrSommets; i++)
            {
                

                Label sommet = new Label();

                sommet.Content = i.ToString();
                sommet.VerticalAlignment = VerticalAlignment.Center;
                sommet.HorizontalAlignment = HorizontalAlignment.Center;
                sommet.FontSize = 15;
                sommet.FontWeight = FontWeights.UltraBold;
                sommet.Height = 50;
                sommet.Width = 50;
                sommet.BorderBrush = Brushes.Black;

               /* Canvas.SetTop(sommet, y1 + centerY);
                Canvas.SetLeft(sommet, x1 + centerX);
                x1 = x2; y1 = y2;*/

                sommets.Add(sommet);

            }

            for (int i1 = 0, i2 = 0; i1 < nbrSommets; i1++, i2 += nbrSommets)
            {
                for (int j1 = (i2 + i1), j2 = i1; j2 < nbrSommets; j1++, j2++)
                {

                    if (matrice.ElementAt<int>(j1) > 0)
                    {

                        double xl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.LeftProperty);
                        double yl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.TopProperty);
                        double xl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.LeftProperty);
                        double yl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.TopProperty);
                        Line arret = new Line()
                        {
                            X1 = xl1 + 25,
                            X2 = xl2 + 25,
                            Y1 = yl1 + 25,
                            Y2 = yl2 + 25,
                            Stroke = Brushes.Black,
                            StrokeThickness = 3
                        };
                        arrets.Add(arret);
                        Label valeur = new Label();
                        valeur.VerticalAlignment = VerticalAlignment.Center;
                        valeur.HorizontalAlignment = HorizontalAlignment.Center;
                        valeur.FontSize = 15;
                        valeur.FontWeight = FontWeights.UltraBold;
                        valeur.Height = 50;
                        valeur.Width = 50;
                        valeur.BorderBrush = Brushes.Black;
                        Canvas.SetLeft(valeur, (xl1 + xl2) / 2);
                        Canvas.SetTop(valeur, (yl1 + yl2) / 2);
                        valeur.Content = Convert.ToString(matrice[j1]);
                        Valeurs.Add(valeur);
                    }
                }
            }
        }
        public static void SommetArretePositionGNO(List<int> matrice, ref List<Line> arrets, ref List<Label> sommets,ref List<Label> Valeurs)
        {
            int nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            double angel = 360 / nbrSommets * (Math.PI / 180);
            double x1 = nbrSommets / 2 * 100, x2;
            double y1 = 0, y2;
            double size = (x1 * 2 + 200);
            double centerX = size / 2;
            double centerY = size / 2;

            for (int i = 0; i < nbrSommets; i++)
            {
                x2 = x1 * Math.Cos(angel) - y1 * Math.Sin(angel);
                y2 = x1 * Math.Sin(angel) + y1 * Math.Cos(angel);

                Label sommet = new Label();

                sommet.Content = i.ToString();
                sommet.VerticalAlignment = VerticalAlignment.Center;
                sommet.HorizontalAlignment = HorizontalAlignment.Center;
                sommet.FontSize = 15;
                sommet.FontWeight = FontWeights.UltraBold;
                sommet.Height = 50;
                sommet.Width = 50;
                sommet.BorderBrush = Brushes.Black;

                Canvas.SetTop(sommet, y1 + centerY);
                Canvas.SetLeft(sommet, x1 + centerX);
                x1 = x2; y1 = y2;

                sommets.Add(sommet);

            }

            for (int i1 = 0, i2 = 0; i1 < nbrSommets; i1++, i2 += nbrSommets)
            {
                for (int j1 = (i2 + i1), j2 = i1; j2 < nbrSommets; j1++, j2++)
                {

                    if (matrice.ElementAt<int>(j1) > 0)
                    {

                        double xl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.LeftProperty);
                        double yl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.TopProperty);
                        double xl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.LeftProperty);
                        double yl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.TopProperty);
                        Line arret = new Line()
                        {
                            X1 = xl1 + 25,
                            X2 = xl2 + 25,
                            Y1 = yl1 + 25,
                            Y2 = yl2 + 25,
                            Stroke = Brushes.Black,
                            StrokeThickness = 3
                        };
                        arrets.Add(arret);
                        Label valeur = new Label();
                        valeur.VerticalAlignment = VerticalAlignment.Center;
                        valeur.HorizontalAlignment = HorizontalAlignment.Center;
                        valeur.FontSize = 15;
                        valeur.FontWeight = FontWeights.UltraBold;
                        /*valeur.Height = 50;
                        valeur.Width = 50;*/
                        valeur.Name = "V" + Convert.ToString(j1);
                        valeur.BorderBrush = Brushes.Black;
                        Canvas.SetLeft(valeur,(xl1+xl2)/2);
                        Canvas.SetTop(valeur, (yl1 + yl2) / 2);
                        valeur.Content = Convert.ToString(matrice[j1]);
                        Valeurs.Add(valeur);
                    }
                }
            }
        }

        public static void ColorerSommets(ref List<Label> sommets, Brush color)
        {
            foreach (Label i in sommets)
            {
                i.Background = color;
            }
        }

        public static void StylerSommets(ref List<Label> sommets, String template)
        {
            foreach (Label i in sommets)
            {
                i.SetResourceReference(Window.TemplateProperty, template);
            }
        }
    }
}
