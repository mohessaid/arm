﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TPRAM
{
    public static class ImplimentationDesAlgorithmes
    {

        public static bool Kayn(List<int> i, int elem, ref int index)
        {
            foreach (int j in i)
            {
                if (elem == j)
                {
                    return true;
                }

                index++;
            }
            return false;
        }

        public static bool Circiut(int depart, int sommet, ref Dictionary<List<int>, int> dic)
        {
            if (depart == sommet) // La condition d'arrete si le depart == le sommet
            {
                return true;
            }
            else
            {
                Dictionary<List<int>, int> keys0 = new Dictionary<List<int>, int>();
                Dictionary<List<int>, int> keys1 = new Dictionary<List<int>, int>();
                bool exist = false;
                //foreach (KeyValuePair<List<int>, int> c in dic)
                
                for (int i = 0; i < dic.Count; i++)
                {
                    if (dic.Keys.ElementAt<List<int>>(i)[0] == sommet)
                    {
                        //nextSommet = dic.Keys.ElementAt<List<int>>(i)[1];
                        keys0.Add(dic.Keys.ElementAt<List<int>>(i), dic.Values.ElementAt<int>(i));
                        //dic.Remove(dic.Keys.ElementAt<List<int>>(i));
                    }
                    else if (dic.Keys.ElementAt<List<int>>(i)[1] == sommet)
                    {
                        //nextSommet = c.Key[0];
                        keys1.Add(dic.Keys.ElementAt<List<int>>(i), dic.Values.ElementAt<int>(i));
                        //dic.Remove(dic.Keys.ElementAt<List<int>>(i));
                    }

                }

                foreach (KeyValuePair<List<int>, int> c in keys0)
                {
                    dic.Remove(c.Key);
                }

                foreach (KeyValuePair<List<int>, int> c in keys0)
                {
                    exist = exist || Circiut(depart, c.Key[1], ref dic);
                    if (exist)
                        break;
                }
                if (!exist)
                {
                    foreach (KeyValuePair<List<int>, int> c in keys0)
                    {
                        dic.Add(c.Key, c.Value);
                    }
                    foreach (KeyValuePair<List<int>, int> c in keys1)
                    {
                        dic.Remove(c.Key);
                    }
                    foreach (KeyValuePair<List<int>, int> c in keys1)
                    {
                        exist = exist || Circiut(depart, c.Key[0], ref dic);
                        if (exist)
                            break;
                    }
                }
                /*if (exist)
                {
                    
                    return Circiut(depart, nextSommet, ref dic);
                    return exist
                }
                else
                {
                    return false;
                }*/
                return exist;
            }
        }

        public static void Kruskal(List<int> matrice, ref List<int> matriceAR, ref String kruskalCout)
        {
            // copier la matrice original dans un nouveau matrice
            matriceAR = new List<int>(matrice); 

            // Le variable qui va contenir les arret dans l'ordre decroissante
            Dictionary<List<int>, int> matKruskal = new Dictionary<List<int>, int>();

            // Calculer le nombre de sommets 
            int numDesSomments = Convert.ToInt32(Math.Sqrt(matrice.Count));

            // Le variable qui va contenir les arret qui construire l'arbre ARM
            Dictionary<List<int>, int> arbreKruskale = new Dictionary<List<int>, int>();

            // Mettez tous les arret dans un dictionare de la forme [(ligne, col), val]    
            for (int i = 0, i1 = 0; i < matrice.Count; i += numDesSomments, i1++)
            {
                for (int j = i1 + 1; j < numDesSomments; j++)
                {
                    if (matrice[i + j] != 0)
                    {
                        List<int> arret = new List<int>() { i1, j };
                        matKruskal.Add(arret, matrice[j + i]);
                    }
                }
            }

            // Ordonneé la matrice
            matKruskal = (from entry in matKruskal orderby entry.Value ascending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);
            
            
             
            
            // Partié principale de l'algorithme
            List<int> sommet = new List<int>(); // la liste des sommets
            for (int i = 0; i < numDesSomments; i++)
            {
                sommet.Add(i);
            }

            int x = 0;
            int nombreArret = 1;
            while (nombreArret < (numDesSomments))
            {
                List<int> elemKey = matKruskal.Keys.ElementAt<List<int>>(x);
                int elemValue = matKruskal.Values.ElementAt<int>(x);

                /* if (sommet.Count != 0)
                 {
                     int c1 = 0; // l'indicd de sommet en ligne
                     int c2 = 0; // l'indice de sommet en colone

                     if (Kayn(sommet, elemKey[0], ref c1) && Kayn(sommet, elemKey[1], ref c2))
                     {
                         arbreKruskale.Add(elemKey, elemValue);
                         nombreArret++;


                         sommet.RemoveAt(c1);
                         if (c1 > c2)
                             sommet.RemoveAt(c2);
                         else
                             sommet.RemoveAt(c2 - 1);

                     }
                     else if (Kayn(sommet, elemKey[0], ref c1))
                     {
                         arbreKruskale.Add(elemKey, elemValue);
                         nombreArret++;
                         sommet.RemoveAt(c1);
                     }
                     else if (Kayn(sommet, elemKey[1], ref c2))
                     {
                         arbreKruskale.Add(elemKey, elemValue);
                         nombreArret++;
                         sommet.RemoveAt(c2);
                     }
                 }
                 else
                 {*/
                Dictionary<List<int>, int> dic0 = new Dictionary<List<int>, int>(arbreKruskale);
                Dictionary<List<int>, int> dic1 = new Dictionary<List<int>, int>(arbreKruskale);

                /*foreach (KeyValuePair<List<int>, int> c in arbreKruskale)
                {
                    dic.Add(c.Key, c.Value);
                }*/
                bool circiutExist = false;
                /*for (int i = 0; i < dic.Count; i++)
                //foreach (KeyValuePair<List<int>, int> c in dic)
                {
                    if ((elemKey[1] == dic.Keys.ElementAt<List<int>>(i)[0]) || (elemKey[1] == dic.Keys.ElementAt<List<int>>(i)[1]))
                    {
                        if (Circiut(elemKey[0], elemKey[1], ref dic))
                        {
                            circiutExist = true;
                            break;
                        }
                        else
                        {
                            i = 0;
                        }
                    }
                    else if((elemKey[0] == dic.Keys.ElementAt<List<int>>(i)[0]) || (elemKey[0] == dic.Keys.ElementAt<List<int>>(i)[1]))
                    {
                        if (Circiut(elemKey[1], elemKey[0], ref dic))
                        {
                            circiutExist = true;
                            break;
                        }
                        else
                        {
                            i = 0;
                        }
                    }
                }*/

                if (!Circiut(elemKey[0], elemKey[1], ref dic0))
                {
                    arbreKruskale.Add(elemKey, elemValue);
                    nombreArret++;
                }
                /*else if (!Circiut(elemKey[1], elemKey[0], ref dic1))
                {
                    arbreKruskale.Add(elemKey, elemValue);
                    nombreArret++;
                }*/
                /*}*/
                x++;
            }


            List<int> indice = new List<int>();
            int cout = 0;
            // trouver la matrice de 
            for (int i = 0; i < arbreKruskale.Keys.Count; i++)
            {
                List<int> elemKey = arbreKruskale.Keys.ElementAt<List<int>>(i);
                indice.Add(elemKey[0] * numDesSomments + elemKey[1]);
                cout += arbreKruskale.Values.ElementAt<int>(i);
            }

            kruskalCout = Convert.ToString(cout);

            indice.Sort();
            for (int i = 0; i < matriceAR.Count; i++)
            {
                if (indice.Count != 0)
                {
                    if (i == indice[0])
                    {
                        indice.RemoveAt(0);
                    }
                    else
                    {
                        matriceAR[i] = 0;
                    }
                }
                else
                {
                    matriceAR[i] = 0;
                }



            }
            
        }
        public static void KruskalMax(List<int> matrice, ref List<int> matriceARMax, ref String kruskalCout)
        {
            // copier la matrice original dans un nouveau matrice
            matriceARMax = new List<int>(matrice);

            // Le variable qui va contenir les arret dans l'ordre decroissante
            Dictionary<List<int>, int> matKruskal = new Dictionary<List<int>, int>();

            // Calculer le nombre de sommets 
            int numDesSomments = Convert.ToInt32(Math.Sqrt(matrice.Count));

            // Le variable qui va contenir les arret qui construire l'arbre ARM
            Dictionary<List<int>, int> arbreKruskale = new Dictionary<List<int>, int>();

            // Mettez tous les arret dans un dictionare de la forme [(ligne, col), val]    
            for (int i = 0, i1 = 0; i < matrice.Count; i += numDesSomments, i1++)
            {
                for (int j = i1 + 1; j < numDesSomments; j++)
                {
                    if (matrice[i + j] != 0)
                    {
                        List<int> arret = new List<int>() { i1, j };
                        matKruskal.Add(arret, matrice[j + i]);
                    }
                }
            }

            // Ordonneé la matrice

            matKruskal = (from entry in matKruskal orderby entry.Value descending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);
            // Partié principale de l'algorithme
            List<int> sommet = new List<int>(); // la liste des sommets
            for (int i = 0; i < numDesSomments; i++)
            {
                sommet.Add(i);
            }

            int x = 0;
            int nombreArret = 1; //some
            while (nombreArret < (numDesSomments))
            {
                List<int> elemKey = matKruskal.Keys.ElementAt<List<int>>(x);
                int elemValue = matKruskal.Values.ElementAt<int>(x);
                Dictionary<List<int>, int> dic0 = new Dictionary<List<int>, int>(arbreKruskale);
                Dictionary<List<int>, int> dic1 = new Dictionary<List<int>, int>(arbreKruskale);

                if (!Circiut(elemKey[0], elemKey[1], ref dic0))
                {
                    arbreKruskale.Add(elemKey, elemValue);
                    nombreArret++;
                }

                x++;
            }


            List<int> indice = new List<int>();
            int cout = 0;
            // trouver la matrice de 
            for (int i = 0; i < arbreKruskale.Keys.Count; i++)
            {
                List<int> elemKey = arbreKruskale.Keys.ElementAt<List<int>>(i);
                indice.Add(elemKey[0] * numDesSomments + elemKey[1]);
                cout += arbreKruskale.Values.ElementAt<int>(i);
            }

            kruskalCout = Convert.ToString(cout);

            indice.Sort();
            for (int i = 0; i < matriceARMax.Count; i++)
            {
                if (indice.Count != 0)
                {
                    if (i == indice[0])
                    {
                        indice.RemoveAt(0);
                    }
                    else
                    {
                        matriceARMax[i] = 0;
                    }
                }
                else
                {
                    matriceARMax[i] = 0;
                }

            }
        }
        public static void Prim(List<int> matrice, ref List<int> matriceARPrim, ref String primCout)
        {
            List<int> t1 = new List<int>(); // L'ensemble t1 qui contient les sommet
            List<int> t = new List<int>(); // L'ensemeble t qui nous voulons cree au cours d'operation

            // Caluclez le nombre des sommets
            int numDesSommets = Convert.ToInt32(Math.Sqrt(matrice.Count));

            // Mettez tous les sommet dans l'ensemeble t1
            for (int i = 0; i < numDesSommets; i++)
            {
                t1.Add(i);
            }

            // L'ensemble des arrets qui compose l'arabre recoverente minimum
            Dictionary<List<int>, int> arbreARM = new Dictionary<List<int>, int>();

            int min = 1000000000; // qui represente le minimum
            int indice = 0; // l'indice dans la list des valeur
            int col = 0; // l'indice des colone
            int ligne = 0; // l'indice des ligne

            // Trouver le plus petit element dans la matrice
            for (int i = 0, c = matrice.Count; i < c; i++)
            {
                if ((matrice[i] < min) && (matrice[i] != 0))
                {
                    min = matrice[i];
                    indice = i;
                }
            }

            // Trouver la position de ce petit element [ligne, col]
            if (indice > numDesSommets)
                ligne = Convert.ToInt32(Math.Floor((double)indice / numDesSommets));

            col = indice - ligne * numDesSommets;

            // Marquer les deux sommet ou l'arret trouver et mettez les deux sommet dans l'ensemble t
            arbreARM.Add(new List<int>() { ligne, col }, min);
            t1.Remove(ligne); t.Add(ligne);
            t1.Remove(col); t.Add(col);
 
            // Commencer l'operation de marquage sur la matrice
            while (t1.Count != 0)
            {
                min = 1000000000; // qui represente le minimum
                col = 0; // l'indice des colone
                ligne = 0; // l'indice des ligne

                // Chercher le minimum entre les arret de t vers t1
                foreach (int i in t)
                {
                    foreach (int j in t1)
                    {
                        if ((matrice[i * numDesSommets + j] < min) && (matrice[i * numDesSommets + j] != 0))
                        {
                            min = matrice[i * numDesSommets + j];
                            ligne = i;
                            col = j;
                        }
                    }
                }
                
                // Chercher le minimum entre les arret de t1 vers t
                foreach (int i in t1)
                {
                    foreach (int j in t)
                    {
                        if ((matrice[i * numDesSommets + j] < min) && (matrice[i * numDesSommets + j] != 0))
                        {
                            min = matrice[i * numDesSommets + j];
                            ligne = i;
                            col = j;
                        }
                    }
                }

                arbreARM.Add(new List<int>() { ligne, col }, min);
                if (!t.Contains(ligne))
                {
                    t1.Remove(ligne); t.Add(ligne);
                }
                else
                {
                    t1.Remove(col); t.Add(col);
                }
            }

            // Calculez tous les indices d'apres les cles de dictionare
            List<int> indices = new List<int>();
            int cout = 0;
            // trouver la matrice de 
            for (int i = 0; i < arbreARM.Keys.Count; i++)
            {
                List<int> elemKey = arbreARM.Keys.ElementAt<List<int>>(i);
                indices.Add(elemKey[0] * numDesSommets + elemKey[1]);
                cout += arbreARM.Values.ElementAt<int>(i);
            }

            primCout = Convert.ToString(cout);

            indices.Sort();
            matriceARPrim = new List<int>(matrice);
            for (int i = 0; i < matriceARPrim.Count; i++)
            {
                if (indices.Count != 0)
                {
                    if (i == indices[0])
                    {
                        indices.RemoveAt(0);
                    }
                    else
                    {
                        matriceARPrim[i] = 0;
                    }
                }
                else
                {
                    matriceARPrim[i] = 0;
                }



            }
        }
        public static void PrimMax(List<int> matrice, ref List<int> matriceARPrimMax, ref String primCout)
        {
            List<int> t1 = new List<int>(); // L'ensemble t1 qui contient les sommet
            List<int> t = new List<int>(); // L'ensemeble t qui nous voulons cree au cours d'operation

            // Caluclez le nombre des sommets
            int numDesSommets = Convert.ToInt32(Math.Sqrt(matrice.Count));

            // Mettez tous les sommet dans l'ensemeble t1
            for (int i = 0; i < numDesSommets; i++)
            {
                t1.Add(i);
            }

            // L'ensemble des arrets qui compose l'arabre recoverente minimum
            Dictionary<List<int>, int> arbreARM = new Dictionary<List<int>, int>();
            // trouver l'arbre Min de prim
            int max = 0; // qui represente le minimum
            int indice = 0; // l'indice dans la list des valeur
            int col = 0; // l'indice des colone
            int ligne = 0; // l'indice des ligne

            // Trouver le plus petit element dans la matrice
            for (int i = 0, c = matrice.Count; i < c; i++)
            {
                if ((matrice[i] > max) && (matrice[i] != 0))
                {
                    max = matrice[i];
                    indice = i;
                }
            }

            // Trouver la position de ce petit element [ligne, col]
            if (indice > numDesSommets)
                ligne = Convert.ToInt32(Math.Floor((double)indice / numDesSommets));

            col = indice - ligne * numDesSommets;

            // Marquer les deux sommet ou l'arret trouver et mettez les deux sommet dans l'ensemble t
            arbreARM.Add(new List<int>() { ligne, col }, max);
            t1.Remove(ligne); t.Add(ligne);
            t1.Remove(col); t.Add(col);

            // Commencer l'operation de marquage sur la matrice
            while (t1.Count != 0)
            {
                max = 0; // qui represente le minimum
                col = 0; // l'indice des colone
                ligne = 0; // l'indice des ligne

                // Chercher le minimum entre les arret de t vers t1
                foreach (int i in t)
                {
                    foreach (int j in t1)
                    {
                        if ((matrice[i * numDesSommets + j] > max) && (matrice[i * numDesSommets + j] != 0))
                        {
                            max = matrice[i * numDesSommets + j];
                            ligne = i;
                            col = j;
                        }
                    }
                }

                // Chercher le minimum entre les arret de t1 vers t
                foreach (int i in t1)
                {
                    foreach (int j in t)
                    {
                        if ((matrice[i * numDesSommets + j] > max) && (matrice[i * numDesSommets + j] != 0))
                        {
                            max = matrice[i * numDesSommets + j];
                            ligne = i;
                            col = j;
                        }
                    }
                }

                arbreARM.Add(new List<int>() { ligne, col }, max);
                if (!t.Contains(ligne))
                {
                    t1.Remove(ligne); t.Add(ligne);
                }
                else
                {
                    t1.Remove(col); t.Add(col);
                }
            }

            // Calculez tous les indices d'apres les cles de dictionare
            List<int> indices = new List<int>();
            int cout = 0;
            // trouver la matrice de 
            for (int i = 0; i < arbreARM.Keys.Count; i++)
            {
                List<int> elemKey = arbreARM.Keys.ElementAt<List<int>>(i);
                indices.Add(elemKey[0] * numDesSommets + elemKey[1]);
                cout += arbreARM.Values.ElementAt<int>(i);
            }

            primCout = Convert.ToString(cout);

            indices.Sort();
            matriceARPrimMax = new List<int>(matrice);
            for (int i = 0; i < matriceARPrimMax.Count; i++)
            {
                if (indices.Count != 0)
                {
                    if (i == indices[0])
                    {
                        indices.RemoveAt(0);
                    }
                    else
                    {
                        matriceARPrimMax[i] = 0;
                    }
                }
                else
                {
                    matriceARPrimMax[i] = 0;
                }



            }
        }

        public static void Sollin(List<int> matrice, ref List<int> matriceAR, ref String sollinCout)
        {
            // copier la matrice original dans un nouveau matrice
            matriceAR = new List<int>(matrice);

            // Le variable qui va contenir les arret dans l'ordre decroissante
            Dictionary<List<int>, int> matSollin = new Dictionary<List<int>, int>();
            Dictionary<List<int>, int> matSollinV1 = new Dictionary<List<int>, int>();

            // Calculer le nombre de sommets 
            int numDesSomments = Convert.ToInt32(Math.Sqrt(matrice.Count));

            // Le variable qui va contenir les arret qui construire l'arbre ARM
            Dictionary<List<int>, int> arbreSollin = new Dictionary<List<int>, int>();

            // Mettez tous les arret dans un dictionare de la forme [(ligne, col), val]    
            for (int i = 0, i1 = 0; i < matrice.Count; i += numDesSomments, i1++)
            {
                for (int j = i1 + 1; j < numDesSomments; j++)
                {
                    if (matrice[i + j] != 0)
                    {
                        List<int> arret = new List<int>() { i1, j };
                        matSollin.Add(arret, matrice[j + i]);
                    }
                }
            }

            matSollinV1 = new Dictionary<List<int>, int>(matSollin);

            // Ordonneé la matrice
            //matKruskal = (from entry in matKruskal orderby entry.Value ascending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);




            // Partié principale de l'algorithme
            List<int> sommet = new List<int>(); // la liste des sommets
            for (int i = 0; i < numDesSomments; i++)
            {
                sommet.Add(i);
            }
            int minValue = matSollin.Values.Min();
            int indiceMin = 0;
            for (int i = 0; i < matSollin.Values.Count; i++)
            {
                if (matSollin.Values.ElementAt<int>(i) == minValue)
                {
                    indiceMin = i;
                    break;
                }
            }
            arbreSollin.Add(matSollin.Keys.ElementAt<List<int>>(indiceMin), matSollin.Values.ElementAt<int>(indiceMin));
            sommet.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin)[0]);
            sommet.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin)[1]);
            matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin));
            matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMin));
            
            while (sommet.Count != 0)
            {
                minValue = matSollin.Values.Min();
                indiceMin = 0;
                for (int i = 0; i < matSollin.Values.Count; i++)
                {
                    if (matSollin.Values.ElementAt<int>(i) == minValue)
                    {
                        indiceMin = i;
                        break;
                    }
                }

                List<int> elemKey = matSollin.Keys.ElementAt<List<int>>(indiceMin);
                int elemValue = matSollin.Values.ElementAt<int>(indiceMin);
                
                if (sommet.Count != 0)
                {
                    int c1 = 0; // l'indicd de sommet en ligne
                    int c2 = 0; // l'indice de sommet en colone
                    
                    if (Kayn(sommet, elemKey[0], ref c1) && Kayn(sommet, elemKey[1], ref c2))
                    {
                        arbreSollin.Add(elemKey, elemValue);
                        sommet.Remove(elemKey[0]);
                        sommet.Remove(elemKey[1]);
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin));
                        matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMin));

                        /* sommet.RemoveAt(c1);
                            if (c1 > c2)
                                //sommet.RemoveAt(c2);
                            
                            else
                                sommet.RemoveAt(c2 - 1);*/


                    }
                    else if (Kayn(sommet, elemKey[0], ref c1))
                    {
                        arbreSollin.Add(elemKey, elemValue);
                        sommet.Remove(elemKey[0]);
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin));
                        matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMin));
                        //sommet.RemoveAt(c1);

                    }
                    else if (Kayn(sommet, elemKey[1], ref c2))
                    {
                        arbreSollin.Add(elemKey, elemValue);
                        sommet.Remove(elemKey[1]);
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin));
                        matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMin));
                        //sommet.RemoveAt(c2);

                    }

                    else
                    {
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMin));
                    }
                }
                
                



                /*if (!Circiut(elemKey[0], elemKey[1], ref dic0))
                {
                    arbreKruskale.Add(elemKey, elemValue);
                    nombreArret++;
                }*/
                
                
               
            }


            List<int> indice = new List<int>();
            int cout = 0;
            // trouver la matrice de 
            for (int i = 0; i < arbreSollin.Keys.Count; i++)
            {
                List<int> elemKey = arbreSollin.Keys.ElementAt<List<int>>(i);
                indice.Add(elemKey[0] * numDesSomments + elemKey[1]);
                cout += arbreSollin.Values.ElementAt<int>(i);
            }

            sollinCout = Convert.ToString(cout);

            indice.Sort();
            for (int i = 0; i < matriceAR.Count; i++)
            {
                if (indice.Count != 0)
                {
                    if (i == indice[0])
                    {
                        indice.RemoveAt(0);
                    }
                    else
                    {
                        matriceAR[i] = 0;
                    }
                }
                else
                {
                    matriceAR[i] = 0;
                }

            }
            Graphe sGR = new Graphe(numDesSomments);
            sGR.Matrice = matriceAR;
            sGR.CConnexe();
            
            //System.Windows.MessageBox.Show(Convert.ToString(sGR.NbrdeselementConnex));
            if (sGR.NbrdeselementConnex != 1)
            {
                Dictionary<int, List<int>> gReduit = new Dictionary<int, List<int>>();
                List<int> relationEC = new List<int>();
                List<int> matriceARV1 = new List<int>();
                for (int i = 0, c = sGR.NbrdeselementConnex * sGR.NbrdeselementConnex; i < c; i++)
                {
                    relationEC.Add(0);
                }
                String sRCout = "";

                //System.Windows.MessageBox.Show("Hello I am in this sollin");
                // Trouver le graphe reduit pour appliquer l'algorithme de Sollin sur leur matrice encour une fois
                int indexEC = 0;
                foreach (List<int> eC in sGR.ElementsConnex)
                {
                    int indexEC1 = 0;
                    foreach (List<int> eC1 in sGR.ElementsConnex)
                    {
                        if (eC != eC1)
                        {
                            int min = 10000000;
                            foreach (int eCS in eC)
                            {
                                foreach (int eCS1 in eC1)
                                {
                                    if ((matrice[numDesSomments * eCS + eCS1] > 0) && (min > matrice[numDesSomments * eCS + eCS1]))
                                    {
                                        min = matrice[numDesSomments * eCS + eCS1];
                                        relationEC[indexEC * sGR.NbrdeselementConnex + indexEC1] = matrice[numDesSomments * eCS + eCS1];
                                        if (!gReduit.ContainsKey(indexEC * sGR.NbrdeselementConnex + indexEC1))
                                            gReduit.Add(indexEC * sGR.NbrdeselementConnex + indexEC1, new List<int>() { eCS, eCS1 });
                                        else 
                                        {
                                            gReduit[indexEC * sGR.NbrdeselementConnex + indexEC1] = new List<int>() { eCS, eCS1 };
                                        }
                                    }
                                }
                            }
                        }
                        indexEC1++;
                    }

                    indexEC++;
                } // Fin d'operation pour trouver le graphe reduit

                Sollin(relationEC, ref matriceARV1, ref sRCout);
                //int indexGR = 0;
                
                foreach (int i in gReduit.Keys)
                {
                    Dictionary<List<int>, int> dic = new Dictionary<List<int>, int>(arbreSollin);
                    if((matriceARV1[i] > 0 ) && (!Circiut(gReduit[i].ElementAt<int>(0), gReduit[i].ElementAt<int>(1), ref dic)))
                    {

                        arbreSollin.Add(gReduit[i], matriceARV1[i] );
                    }
                    //indexGR++;
                }

                cout = 0;
                // trouver la matrice de 
                for (int i = 0; i < arbreSollin.Keys.Count; i++)
                {
                    List<int> elemKey = arbreSollin.Keys.ElementAt<List<int>>(i);
                    indice.Add(elemKey[0] * numDesSomments + elemKey[1]);
                    cout += arbreSollin.Values.ElementAt<int>(i);
                }
                matriceAR = new List<int>(matrice);
                sollinCout = Convert.ToString(cout);

                indice.Sort();
                for (int i = 0; i < matriceAR.Count; i++)
                {
                    if (indice.Count != 0)
                    {
                        if (i == indice[0])
                        {
                            indice.RemoveAt(0);
                        }
                        else
                        {
                            matriceAR[i] = 0;
                        }
                    }
                    else
                    {
                        matriceAR[i] = 0;
                    }

                }
            }

        }

        public static void SollinMax(List<int> matrice, ref List<int> matriceAR, ref String sollinCout)
        {
            // copier la matrice original dans un nouveau matrice
            matriceAR = new List<int>(matrice);

            // Le variable qui va contenir les arret dans l'ordre decroissante
            Dictionary<List<int>, int> matSollin = new Dictionary<List<int>, int>();
            Dictionary<List<int>, int> matSollinV1 = new Dictionary<List<int>, int>();

            // Calculer le nombre de sommets 
            int numDesSomments = Convert.ToInt32(Math.Sqrt(matrice.Count));

            // Le variable qui va contenir les arret qui construire l'arbre ARM
            Dictionary<List<int>, int> arbreSollin = new Dictionary<List<int>, int>();

            // Mettez tous les arret dans un dictionare de la forme [(ligne, col), val]    
            for (int i = 0, i1 = 0; i < matrice.Count; i += numDesSomments, i1++)
            {
                for (int j = i1 + 1; j < numDesSomments; j++)
                {
                    if (matrice[i + j] != 0)
                    {
                        List<int> arret = new List<int>() { i1, j };
                        matSollin.Add(arret, matrice[j + i]);
                    }
                }
            }

            matSollinV1 = new Dictionary<List<int>, int>(matSollin);

            // Ordonneé la matrice
            //matKruskal = (from entry in matKruskal orderby entry.Value ascending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);




            // Partié principale de l'algorithme
            List<int> sommet = new List<int>(); // la liste des sommets
            for (int i = 0; i < numDesSomments; i++)
            {
                sommet.Add(i);
            }
            int MaxValue = matSollin.Values.Max();
            int indiceMax = 0;
            for (int i = 0; i < matSollin.Values.Count; i++)
            {
                if (matSollin.Values.ElementAt<int>(i) == MaxValue)
                {
                    indiceMax = i;
                    break;
                }
            }
            arbreSollin.Add(matSollin.Keys.ElementAt<List<int>>(indiceMax), matSollin.Values.ElementAt<int>(indiceMax));
            sommet.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax)[0]);
            sommet.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax)[1]);
            matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax));
            matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMax));

            while (sommet.Count != 0)
            {
                MaxValue = matSollin.Values.Max();
                indiceMax = 0;
                for (int i = 0; i < matSollin.Values.Count; i++)
                {
                    if (matSollin.Values.ElementAt<int>(i) == MaxValue)
                    {
                        indiceMax = i;
                        break;
                    }
                }

                List<int> elemKey = matSollin.Keys.ElementAt<List<int>>(indiceMax);
                int elemValue = matSollin.Values.ElementAt<int>(indiceMax);

                if (sommet.Count != 0)
                {
                    int c1 = 0; // l'indicd de sommet en ligne
                    int c2 = 0; // l'indice de sommet en colone

                    if (Kayn(sommet, elemKey[0], ref c1) && Kayn(sommet, elemKey[1], ref c2))
                    {
                        arbreSollin.Add(elemKey, elemValue);
                        sommet.Remove(elemKey[0]);
                        sommet.Remove(elemKey[1]);
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax));
                        matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMax));

                        /* sommet.RemoveAt(c1);
                            if (c1 > c2)
                                //sommet.RemoveAt(c2);
                            
                            else
                                sommet.RemoveAt(c2 - 1);*/


                    }
                    else if (Kayn(sommet, elemKey[0], ref c1))
                    {
                        arbreSollin.Add(elemKey, elemValue);
                        sommet.Remove(elemKey[0]);
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax));
                        matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMax));
                        //sommet.RemoveAt(c1);

                    }
                    else if (Kayn(sommet, elemKey[1], ref c2))
                    {
                        arbreSollin.Add(elemKey, elemValue);
                        sommet.Remove(elemKey[1]);
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax));
                        matSollinV1.Remove(matSollinV1.Keys.ElementAt<List<int>>(indiceMax));
                        //sommet.RemoveAt(c2);

                    }

                    else
                    {
                        matSollin.Remove(matSollin.Keys.ElementAt<List<int>>(indiceMax));
                    }
                }





                /*if (!Circiut(elemKey[0], elemKey[1], ref dic0))
                {
                    arbreKruskale.Add(elemKey, elemValue);
                    nombreArret++;
                }*/



            }


            List<int> indice = new List<int>();
            int cout = 0;
            // trouver la matrice de 
            for (int i = 0; i < arbreSollin.Keys.Count; i++)
            {
                List<int> elemKey = arbreSollin.Keys.ElementAt<List<int>>(i);
                indice.Add(elemKey[0] * numDesSomments + elemKey[1]);
                cout += arbreSollin.Values.ElementAt<int>(i);
            }

            sollinCout = Convert.ToString(cout);

            indice.Sort();
            for (int i = 0; i < matriceAR.Count; i++)
            {
                if (indice.Count != 0)
                {
                    if (i == indice[0])
                    {
                        indice.RemoveAt(0);
                    }
                    else
                    {
                        matriceAR[i] = 0;
                    }
                }
                else
                {
                    matriceAR[i] = 0;
                }

            }
            Graphe sGR = new Graphe(numDesSomments);
            sGR.Matrice = matriceAR;
            sGR.CConnexe();

            //System.Windows.MessageBox.Show(Convert.ToString(sGR.NbrdeselementConnex));
            if (sGR.NbrdeselementConnex != 1)
            {
                Dictionary<int, List<int>> gReduit = new Dictionary<int, List<int>>();
                List<int> relationEC = new List<int>();
                List<int> matriceARV1 = new List<int>();
                for (int i = 0, c = sGR.NbrdeselementConnex * sGR.NbrdeselementConnex; i < c; i++)
                {
                    relationEC.Add(0);
                }
                String sRCout = "";

                //System.Windows.MessageBox.Show("Hello I am in this sollin");
                // Trouver le graphe reduit pour appliquer l'algorithme de Sollin sur leur matrice encour une fois
                int indexEC = 0;
                foreach (List<int> eC in sGR.ElementsConnex)
                {
                    int indexEC1 = 0;
                    foreach (List<int> eC1 in sGR.ElementsConnex)
                    {
                        if (eC != eC1)
                        {
                            int Max = 0;
                            foreach (int eCS in eC)
                            {
                                foreach (int eCS1 in eC1)
                                {
                                    if ((matrice[numDesSomments * eCS + eCS1] > 0) && (Max < matrice[numDesSomments * eCS + eCS1]))
                                    {
                                        Max = matrice[numDesSomments * eCS + eCS1];
                                        relationEC[indexEC * sGR.NbrdeselementConnex + indexEC1] = matrice[numDesSomments * eCS + eCS1];
                                        if (!gReduit.ContainsKey(indexEC * sGR.NbrdeselementConnex + indexEC1))
                                            gReduit.Add(indexEC * sGR.NbrdeselementConnex + indexEC1, new List<int>() { eCS, eCS1 });
                                        else
                                        {
                                            gReduit[indexEC * sGR.NbrdeselementConnex + indexEC1] = new List<int>() { eCS, eCS1 };
                                        }
                                    }
                                }
                            }
                        }
                        indexEC1++;
                    }

                    indexEC++;
                } // Fin d'operation pour trouver le graphe reduit

                SollinMax(relationEC, ref matriceARV1, ref sRCout);
                //int indexGR = 0;

                foreach (int i in gReduit.Keys)
                {
                    Dictionary<List<int>, int> dic = new Dictionary<List<int>, int>(arbreSollin);
                    if ((matriceARV1[i] > 0) && (!Circiut(gReduit[i].ElementAt<int>(0), gReduit[i].ElementAt<int>(1), ref dic)))
                    {

                        arbreSollin.Add(gReduit[i], matriceARV1[i]);
                    }
                    //indexGR++;
                }

                cout = 0;
                // trouver la matrice de 
                for (int i = 0; i < arbreSollin.Keys.Count; i++)
                {
                    List<int> elemKey = arbreSollin.Keys.ElementAt<List<int>>(i);
                    indice.Add(elemKey[0] * numDesSomments + elemKey[1]);
                    cout += arbreSollin.Values.ElementAt<int>(i);
                }
                matriceAR = new List<int>(matrice);
                sollinCout = Convert.ToString(cout);

                indice.Sort();
                for (int i = 0; i < matriceAR.Count; i++)
                {
                    if (indice.Count != 0)
                    {
                        if (i == indice[0])
                        {
                            indice.RemoveAt(0);
                        }
                        else
                        {
                            matriceAR[i] = 0;
                        }
                    }
                    else
                    {
                        matriceAR[i] = 0;
                    }

                }
            }

        }
        
    }
}                                                                                                                                   
