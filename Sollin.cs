﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TPRAM
{
    class Sollin
    {
        private List<int> matriceMin;
        private List<int> matriceMax;
        private List<int> matriceARMin;
        private List<int> matriceARMax;
        private String sollinMinTime;
        private String sollinMaxTime;
        private String sollinMinCout;
        private String sollinMaxCout;

        public Sollin(List<int> matriceAR)
        {
            matriceMin = matriceAR;
            matriceMax = matriceAR;
        }

        public List<int> MatriceARMin
        {
            get{return matriceARMin;}
        }

        public List<int> MatriceARMax
        {
            get { return matriceARMax; }
        }

        public String SollinMinTime
        {
            get { return sollinMinTime; }
        }

        public String SollinMaxTime
        {
            get { return sollinMaxTime; }
        }

        public String SollinMinCout
        {
            get { return sollinMinCout; }
        }

        public String SollinMaxCout
        {
            get { return sollinMaxCout; }
        }

        public void ExecuterSollinMinAlgorithm()
        {
            matriceARMin = new List<int>();
            sollinMinTime = "";
            sollinMinCout = "";
            Stopwatch stopWatchSMin = new Stopwatch();

            stopWatchSMin.Start();

            ImplimentationDesAlgorithmes.Sollin(matriceMin, ref matriceARMin, ref sollinMinCout);

            stopWatchSMin.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan tsSMin = stopWatchSMin.Elapsed;

            
            sollinMinTime = Convert.ToString(tsSMin.Milliseconds) + " ms";
        }

        public void ExecuterSollinMaxAlgorithm()
        {
            matriceARMax = new List<int>();
            sollinMaxTime = "";
            sollinMaxCout = "";

            Stopwatch stopWatchSMax = new Stopwatch();

            stopWatchSMax.Start();

            ImplimentationDesAlgorithmes.SollinMax(matriceMax, ref matriceARMax, ref sollinMaxCout);


            stopWatchSMax.Stop();

            // Get the elapsed time as a TimeSpan value.
            TimeSpan tsSMax = stopWatchSMax.Elapsed;

            sollinMaxTime = Convert.ToString(tsSMax.Milliseconds) + " ms";
        }
    }
}
